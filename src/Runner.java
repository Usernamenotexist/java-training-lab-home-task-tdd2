
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Phaser;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import by.epamlab.beans.Passenger;
import by.epamlab.containers.ArrivalContainerInstance;
import by.epamlab.containers.Container;
import by.epamlab.containers.DispatchContainerInstance;
import by.epamlab.containers.ElevatorContainerInstance;
import by.epamlab.utils.CountDownLatchInstance;
import by.epamlab.utils.ElevatorInit;
import by.epamlab.utils.ElevatorPhaser;
import by.epamlab.utils.Sequencer;
/**
 * @author Admin
 *
 */
public final class Runner {

    private static final String LOGGER_NAME = "Runner";
    private static final String LOGGER_STATE_START = "STARTING_TRANSPORTATION";
    private static final String LOGGER_STATE_END = "COMPLETION_TRANSPORTATION";
    private static final String PROPERTY_PASSENGERS_NUMBER = "passengersNumber";
    private static final String PROPERTY_ELEVATOR_CAPACITY = "elevatorCapacity";

    private static Logger logger = Logger.getLogger(LOGGER_NAME);
    private static ElevatorInit init = ElevatorInit.getInstance();
    private static final int PASSENGERS_NUMBER =
            Integer.parseInt(init.getProperty(PROPERTY_PASSENGERS_NUMBER));
    private static final int MAX_PASSENGERS_ELEVATOR_CAPACITY =
            Integer.parseInt(init.getProperty(PROPERTY_ELEVATOR_CAPACITY));

    /**
     * @param args args
     */
    public static void main(final String[] args) {
        AtomicInteger currentFloor = new AtomicInteger(1);
        Queue<Integer> evelvatorContainer = new LinkedBlockingQueue<>(MAX_PASSENGERS_ELEVATOR_CAPACITY);
        Sequencer sequencer = new Sequencer();
        final Phaser phaser = new ElevatorPhaser(currentFloor);
        logger.trace(LOGGER_STATE_START);

        for (int i = 1; i <= PASSENGERS_NUMBER; i++) {
          new Passenger(evelvatorContainer, currentFloor, sequencer, phaser).start();
        }

        try {
            CountDownLatch startSignal = CountDownLatchInstance.INSTANCE.getInstance();
            startSignal.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Container elevatorContainer = ElevatorContainerInstance.INSTANCE.getInstance();
        Container dispatchStoryContainer = DispatchContainerInstance.INSTANCE.getInstance();
        Container arrivalStoryContainer = ArrivalContainerInstance.INSTANCE.getInstance();
        if (elevatorContainer.check() && dispatchStoryContainer.check()
                && arrivalStoryContainer.check()) {
            logger.trace(LOGGER_STATE_END);
        } else {
            throw new UnsupportedOperationException("Elevator fail!");
        }

    }

    private Runner() {
    }

}
