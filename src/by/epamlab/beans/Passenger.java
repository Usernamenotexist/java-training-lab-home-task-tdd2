package by.epamlab.beans;
import java.util.Queue;
import java.util.concurrent.Phaser;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import by.epamlab.containers.ArrivalContainerInstance;
import by.epamlab.containers.Container;
import by.epamlab.containers.DispatchContainerInstance;
import by.epamlab.containers.ElevatorContainerInstance;
import by.epamlab.utils.RandomFloorGenerator;
import by.epamlab.utils.Sequencer;

/**
 * @author Admin
 *
 */
public class Passenger implements Runnable {

    private static final int PLACEHOLDER = 42;
    private static final String LOGGER_NAME = "Passenger ";
    private static final String LOGGER_TRACE_BOAT = ">>BOARDING_OF_PASSENGER ";
    private static final String LOGGER_TRACE_DEBOAT = "<<DEBOADING_OF_PASSENGER ";
    private static final String LOGGER_TRACE_STORY = " on story - ";

    private PassengerState passengerState = new PassengerState();
    private Phaser ph;
    private final Queue<Integer> queue;
    private AtomicInteger currentFloor;
    private Container elevatorContainer = ElevatorContainerInstance.INSTANCE.getInstance();
    private Container dispatchStoryContainer = DispatchContainerInstance.INSTANCE.getInstance();
    private Container arrivalStoryContainer = ArrivalContainerInstance.INSTANCE.getInstance();
    private Logger logger;

    /**
     * @param queue elevator
     * @param currentFloor current floor
     * @param sequencer next int
     * @param ph Phaser
     */
    public Passenger(final Queue<Integer> queue, final AtomicInteger currentFloor,
            final Sequencer sequencer, final Phaser ph) {

        this.ph = ph;
        this.queue = queue;
        this.currentFloor = currentFloor;

        passengerState.setPassengerID(sequencer.next());
        passengerState.setStartStory(RandomFloorGenerator.getFloor());
        passengerState.setDestinationStory(RandomFloorGenerator.getAnotherFloor(passengerState.getStartStory()));
        passengerState.setTransportationState(TransportationState.NOT_STARTED);
        dispatchStoryContainer.register(passengerState);
        logger = Logger.getLogger(LOGGER_NAME + passengerState.getPassengerID());
        ph.register();

    }

    /**
     * start thread
     */
    public void start() {
        new Thread(this).start();
    }

    @Override
    public void run() {
        tryIn();
        tryOut();
    }


    void tryOut() {
        while (!(currentFloor.get() == passengerState.getDestinationStory())) {
            ph.arriveAndAwaitAdvance();
        }
        queue.poll();
        passengerState.setTransportationState(TransportationState.COMPLETED);
        elevatorContainer.deregister(passengerState);
        arrivalStoryContainer.register(passengerState);
        logger.trace(LOGGER_TRACE_DEBOAT + passengerState.getPassengerID()
        + LOGGER_TRACE_STORY + currentFloor.get());
        ph.arriveAndDeregister();
    }

    void tryIn() {
        while (passengerState.getTransportationState() == TransportationState.NOT_STARTED) {
            if (currentFloor.get() == passengerState.getStartStory()) {
                if (queue.offer(PLACEHOLDER)) {
                    passengerState.setTransportationState(TransportationState.IN_PROGRESS);
                    dispatchStoryContainer.deregister(passengerState);
                    elevatorContainer.register(passengerState);
                    logger.trace(LOGGER_TRACE_BOAT + passengerState.getPassengerID()
                    + LOGGER_TRACE_STORY + currentFloor.get());
                }
            }
            ph.arriveAndAwaitAdvance();
        }
    }

    /**
     * @return the passengerState
     */
    public PassengerState getPassengerState() {
        return passengerState;
    }
}
