package by.epamlab.beans;

/**
 * @author Admin
 *
 */
public class PassengerState {

    private TransportationState transportationState;
    private int startStory;
    private int destinationStory;
    private int passengerID;

    /**
     * @return the transportationState
     */
    public TransportationState getTransportationState() {
        return transportationState;
    }
    /**
     * @param transportationState the transportationState to set
     */
    public void setTransportationState(final TransportationState transportationState) {
        this.transportationState = transportationState;
    }
    /**
     * @return the startStory
     */
    public int getStartStory() {
        return startStory;
    }
    /**
     * @param startStory the startStory to set
     */
    public void setStartStory(final int startStory) {
        this.startStory = startStory;
    }
    /**
     * @return the destinationStory
     */
    public int getDestinationStory() {
        return destinationStory;
    }
    /**
     * @param destinationStory the destinationStory to set
     */
    public void setDestinationStory(final int destinationStory) {
        this.destinationStory = destinationStory;
    }
    /**
     * @return the passengerID
     */
    public int getPassengerID() {
        return passengerID;
    }
    /**
     * @param passengerID the passengerID to set
     */
    public void setPassengerID(final int passengerID) {
        this.passengerID = passengerID;
    }
}
