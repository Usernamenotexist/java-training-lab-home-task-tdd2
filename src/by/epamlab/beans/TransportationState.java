package by.epamlab.beans;

/**
 * @author Admin
 *
 */
public enum TransportationState {
    /**
     *
     */
    NOT_STARTED,
    /**
     *
     */
    IN_PROGRESS,
    /**
     *
     */
    COMPLETED,
}
