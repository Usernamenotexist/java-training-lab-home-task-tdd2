package by.epamlab.containers;

/**
 * @author Admin
 *
 */
public enum ArrivalContainerInstance {
    /**
     *
     */
    INSTANCE;

    private ArrivalStoryContainer arrivalStoryContainer = new ArrivalStoryContainer();

    /**
     * @return INSTANCE
     */
    public ArrivalStoryContainer getInstance() {
        return arrivalStoryContainer;
    }
}
