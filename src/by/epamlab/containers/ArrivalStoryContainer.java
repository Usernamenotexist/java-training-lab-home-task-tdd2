/**
 *
 */
package by.epamlab.containers;

import java.util.concurrent.atomic.AtomicInteger;

import by.epamlab.beans.PassengerState;
import by.epamlab.beans.TransportationState;
import by.epamlab.utils.ElevatorInit;

/**
 * @author Admin
 *
 */
public class ArrivalStoryContainer implements Container {

    private static final String PROPERTY_PASSENGERS_NUMBER = "passengersNumber";
    private static ElevatorInit init = ElevatorInit.getInstance();
    private static final int PASSENGERS_NUMBER =
            Integer.parseInt(init.getProperty(PROPERTY_PASSENGERS_NUMBER));
    private AtomicInteger passengers = new AtomicInteger(0);

    /* (non-Javadoc)
     * @see by.epamlab.beans.Container#register(by.epamlab.beans.PassengerState)
     */
    @Override
    public void register(final PassengerState passengerState) {
        if (passengerState.getTransportationState() == TransportationState.COMPLETED) {
            passengers.getAndIncrement();
        }
    }

    /* (non-Javadoc)
     * @see by.epamlab.beans.Container#deregister(by.epamlab.beans.PassengerState)
     */
    @Override
    public void deregister(final PassengerState passengerState) {
        throw new UnsupportedOperationException();
    }

    /* (non-Javadoc)
     * @see by.epamlab.beans.Container#check()
     */
    @Override
    public boolean check() {
        return passengers.get() == PASSENGERS_NUMBER;
    }

}
