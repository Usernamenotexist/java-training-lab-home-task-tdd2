package by.epamlab.containers;

import by.epamlab.beans.PassengerState;

/**
 * @author Admin
 *
 */
public interface Container {
    /**
     * @param passengerState pass
     */
    void register(PassengerState passengerState);
    /**
     * @param passengerState pass
     */
    void deregister(PassengerState passengerState);
    /**
     * @return if zero size?
     */
    boolean check();
}
