package by.epamlab.containers;

/**
 * @author Admin
 *
 */
public enum DispatchContainerInstance {
    /**
     *
     */
    INSTANCE;

    private DispatchStoryContainer dispatchStoryContainer = new DispatchStoryContainer();

    /**
     * @return INSTANCE
     */
    public DispatchStoryContainer getInstance() {
        return dispatchStoryContainer;
    }
}
