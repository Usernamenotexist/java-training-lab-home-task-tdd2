/**
 *
 */
package by.epamlab.containers;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;

import by.epamlab.beans.PassengerState;
import by.epamlab.utils.ElevatorInit;

/**
 * @author Admin
 *
 */
public class DispatchStoryContainer implements Container {

    private static final String PROPERTY_FLOOR_NUMBER = "storiesNumber";
    private static ElevatorInit init = ElevatorInit.getInstance();
    private static final int FLOOR_NUMBER =
            Integer.parseInt(init.getProperty(PROPERTY_FLOOR_NUMBER));
    private AtomicInteger[] floors = Collections.nCopies(FLOOR_NUMBER + 1,
            new AtomicInteger(0)).toArray(new AtomicInteger[0]);

    /* (non-Javadoc)
     * @see by.epamlab.beans.Container#register(by.epamlab.beans.PassengerState)
     */
    @Override
    public void register(final PassengerState passengerState) {
        floors[passengerState.getStartStory()].getAndIncrement();
    }

    /* (non-Javadoc)
     * @see by.epamlab.beans.Container#deregister(by.epamlab.beans.PassengerState)
     */
    @Override
    public void deregister(final PassengerState passengerState) {
        floors[passengerState.getStartStory()].getAndDecrement();
    }

    /* (non-Javadoc)
     * @see by.epamlab.beans.Container#check()
     */
    @Override
    public boolean check() {
        AtomicInteger result  = new AtomicInteger(0);
        for (AtomicInteger i : floors) {
            result.getAndAdd(i.get());
        }
        return result.get() == 0;
    }

}
