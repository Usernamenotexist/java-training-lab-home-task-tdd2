package by.epamlab.containers;

import java.util.concurrent.atomic.AtomicInteger;

import by.epamlab.beans.PassengerState;

/**
 * @author Admin
 *
 */
public class ElevatorContainer implements Container {

    private AtomicInteger counter = new AtomicInteger(0);
    @Override
    public void register(final PassengerState passengerState) {
        counter.getAndIncrement();
    }

    @Override
    public void deregister(final PassengerState passengerState) {
        counter.getAndDecrement();
    }

    @Override
    public boolean check() {
        return counter.get() == 0;
    }
}
