package by.epamlab.containers;

/**
 * @author Admin
 *
 */
public enum ElevatorContainerInstance {
    /**
     *
     */
    INSTANCE;

    private ElevatorContainer elevatorContainer = new ElevatorContainer();

    /**
     * @return INSTANCE
     */
    public ElevatorContainer getInstance() {
        return elevatorContainer;
    }
}
