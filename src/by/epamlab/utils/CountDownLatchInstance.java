package by.epamlab.utils;

import java.util.concurrent.CountDownLatch;

/**
 * @author Admin
 *
 */
public enum CountDownLatchInstance {
    /**
     *
     */
    INSTANCE;

    private CountDownLatch startSignal = new CountDownLatch(1);

    /**
     * @return INSTANCE
     */
    public CountDownLatch getInstance() {
        return startSignal;
    }
}
