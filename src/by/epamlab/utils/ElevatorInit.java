package by.epamlab.utils;

/**
 * @author Admin
 *
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Admin
 *
 */
public final class ElevatorInit {

   private static final String PROPERTIES = "app.properties";
   private final Properties configProp = new Properties();

   private ElevatorInit() {
      InputStream in = this.getClass().getClassLoader().getResourceAsStream(PROPERTIES);
      try {
          configProp.load(in);
      } catch (IOException e) {
          e.printStackTrace();
      }
   }

   private static class LazyHolder {
      private static final ElevatorInit INSTANCE = new ElevatorInit();
   }

    /**
    * @return instance
    */
   public static ElevatorInit getInstance() {
      return LazyHolder.INSTANCE;
   }

   /**
    * @param key String
    * @return property
    */
   public String getProperty(final String key) {
      return configProp.getProperty(key);
   }

   /**
    * @param key String
    * @return boolean
    */
   public boolean containsKey(final String key) {
      return configProp.containsKey(key);
   }
}


