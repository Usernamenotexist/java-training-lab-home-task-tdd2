package by.epamlab.utils;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Phaser;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Admin
 *
 */
public class ElevatorPhaser extends Phaser {


    private AtomicInteger currentFloor;


    /**
     * @param currentFloor int
     */
    public ElevatorPhaser(final AtomicInteger currentFloor) {
         this.currentFloor = currentFloor;
    }

    @Override
    protected boolean onAdvance(final int phase, final int parties) {
        currentFloor.set(RandomFloorGenerator.getAnotherFloor(currentFloor.get()));
        if (parties == 0) {
            CountDownLatch startSignal = CountDownLatchInstance.INSTANCE.getInstance();
            startSignal.countDown();
            return true;
        }
        return false;
    }

}
