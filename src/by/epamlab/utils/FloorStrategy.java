package by.epamlab.utils;

/**
 * @author Admin
 *
 */
public interface FloorStrategy {

    /**
     * @return next floor number
     */
    int nextFloor();

}
