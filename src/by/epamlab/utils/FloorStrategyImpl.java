package by.epamlab.utils;

/**
 * @author Admin
 *
 */
public class FloorStrategyImpl implements FloorStrategy {

    @Override
    public int nextFloor() {
        return RandomFloorGenerator.getFloor();
    }
}
