package by.epamlab.utils;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Admin
 *
 */
public final class RandomFloorGenerator {

    private static final String PROPERTY_STORIES_NUMBER = "storiesNumber";
    private static final int MAX_FLOOR =
            Integer.parseInt(ElevatorInit.getInstance().getProperty(PROPERTY_STORIES_NUMBER));

    private RandomFloorGenerator() {
    }

    /**
     * @return int
     */
    public static int getFloor() {
        return newRandom();
    }

    /**
     * @param floor int
     * @return int not equal to floor
     */
    public static int getAnotherFloor(final int floor) {
        int anotherFloor;
        do {
            anotherFloor = newRandom();
        } while (anotherFloor == floor);
        return anotherFloor;
    }

    private static int newRandom() {
        return ThreadLocalRandom.current().nextInt(1, MAX_FLOOR + 1);
    }
}
