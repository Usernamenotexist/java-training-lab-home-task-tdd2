package by.epamlab.utils;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Admin
 *
 */
public class Sequencer {
    private final AtomicInteger sequenceNumber = new AtomicInteger(0);
    /**
     * @return next int
     */
    public int next() {
        return sequenceNumber.getAndIncrement();
    }
}
