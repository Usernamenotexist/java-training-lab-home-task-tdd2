

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import by.epamlab.beans.PassengerTest;
import by.epamlab.containers.*;
import by.epamlab.utils.*;

/**
 * @author Admin
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ElevatorInitTest.class, SequenserTest.class, FloorStrategyImplTest.class,
    RandomFloorGeneratorTest.class, ArrivalContainerInstanceTest.class,
    ArrivalStoryContainerTest.class, DispatchStoryContainerTest.class,
    ElevatorContainerTest.class, PassengerTest.class})
public class AllTests {

}
