package by.epamlab.beans;

import static org.junit.Assert.*;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.spy;
import static org.hamcrest.CoreMatchers.*;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import by.epamlab.utils.ElevatorInit;
import by.epamlab.utils.ElevatorPhaser;
import by.epamlab.utils.Sequencer;

/**
 * @author Admin
 *
 */
public class PassengerTest {

    private Phaser phaser;
    private Passenger p;
    private Sequencer sequencer;
    private AtomicInteger currentFloor;
    private Queue<Integer> evelvatorContainer;
    private static ElevatorInit init = ElevatorInit.getInstance();
    @SuppressWarnings("unused")
    private static final int PASSENGERS_NUMBER =
            Integer.parseInt(init.getProperty("passengersNumber"));
    private static final int MAX_PASSENGERS_ELEVATOR_CAPACITY =
            Integer.parseInt(init.getProperty("elevatorCapacity"));

    /**
    *
    */
   @Before
   public void init() {
       currentFloor = new AtomicInteger(1);
       evelvatorContainer = new LinkedBlockingQueue<>(MAX_PASSENGERS_ELEVATOR_CAPACITY);
       sequencer = new Sequencer();
       phaser = new ElevatorPhaser(currentFloor);
       p = new Passenger(evelvatorContainer, currentFloor, sequencer, phaser);
   }

   /**
   *
   */
  @Test
  public void testQueue() {
      Queue<Integer> ec = new LinkedBlockingQueue<>(1);
      ec.offer(1);
      assertThat(ec.offer(1), is(equalTo(false)));
      ec.poll();
      assertThat(ec.offer(1), is(equalTo(true)));
  }
    /**
     *
     */
    @Test
    public void testWait() {
        Queue<Integer> ec = new LinkedBlockingQueue<>(1);
        ec.offer(1);
        Passenger ps = new Passenger(ec, currentFloor, sequencer, phaser);
        ps.start();
        assertThat("elevator full", ps.getPassengerState().getTransportationState(),
                is(equalTo(TransportationState.NOT_STARTED)));
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertThat("elevator full", ps.getPassengerState().getTransportationState(),
                is(equalTo(TransportationState.NOT_STARTED)));
        ec.poll();
    }

    /**
    *
    */
   @Test
   public void testIn() {

       p.start();

       try {
           TimeUnit.SECONDS.sleep(1);
       } catch (InterruptedException e) {
           e.printStackTrace();
       }

       assertThat("elevator done", p.getPassengerState().getTransportationState(),
               is(equalTo(TransportationState.COMPLETED)));
   }

   /**
   *
   */
  @Test
  public void testOrderTransportationState() {
      Passenger passenger = spy(p);

      InOrder inOrder = inOrder(passenger);
      passenger.start();
      inOrder.verify(passenger).tryIn();
      inOrder.verify(passenger).tryOut();
  }


}
