package by.epamlab.containers;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import by.epamlab.beans.PassengerState;
import by.epamlab.beans.TransportationState;
import by.epamlab.utils.ElevatorInit;

/**
 * @author Admin
 *
 */
public class ArrivalStoryContainerTest {

    private static final String PROPERTY_PASSENGERS_NUMBER = "passengersNumber";
    private static ElevatorInit init = ElevatorInit.getInstance();
    private static final int PASSENGERS_NUMBER =
            Integer.parseInt(init.getProperty(PROPERTY_PASSENGERS_NUMBER));

    /**
     *
     */
    @Test
    public void testCheck() {
        PassengerState passengerState = new PassengerState();
        passengerState.setTransportationState(TransportationState.COMPLETED);
        Container test  = ArrivalContainerInstance.INSTANCE.getInstance();
        for (int i = 1; i <= PASSENGERS_NUMBER; i++) {
            test.register(passengerState);
        }
        assertThat("all passenger arrive", test.check(), is(equalTo(true)));
    }

}
