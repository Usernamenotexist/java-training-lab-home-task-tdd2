package by.epamlab.containers;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Test;

import by.epamlab.beans.PassengerState;

/**
 * @author Admin
 *
 */
public class ElevatorContainerTest {

    /**
     *
     */
    @Test
    public void testCheck() {
        PassengerState passengerState = new PassengerState();
        Container test  = ElevatorContainerInstance.INSTANCE.getInstance();
        test.register(passengerState);
        test.deregister(passengerState);
        assertThat("all passenger gone", test.check(), is(equalTo(true)));
    }

}
