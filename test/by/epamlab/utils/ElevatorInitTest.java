package by.epamlab.utils;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Test;

/**
 * @author Admin
 *
 */
public class ElevatorInitTest {

    private static final int MAX_FLOOR = Integer.parseInt(ElevatorInit.getInstance().getProperty("storiesNumber"));

    /**
     * test
     */
    @Test
    public void testGetProperty() {
        String floor = ElevatorInit.getInstance().getProperty("storiesNumber");
        assertThat("floor number", Integer.parseInt(floor), is(equalTo(MAX_FLOOR)));
    }

}
