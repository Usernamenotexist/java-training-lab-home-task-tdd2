package by.epamlab.utils;


import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Admin
 *
 */
public class FloorStrategyImplTest {

    private FloorStrategy test;
    private static final int MAX_FLOOR = Integer.parseInt(ElevatorInit.getInstance().getProperty("storiesNumber"));

    /**
     *
     */
    @Before
    public void executedBeforeEach() {
        test = new FloorStrategyImpl();
    }

    /**
     *
     */
    @Test
    public void testNextFloorRange() {
        for (int i = 0; i < 100; i++) {
            int floor = test.nextFloor();
            assertThat("floor number", floor, is(greaterThan(0)));
            assertThat("floor number", floor, is(lessThan(MAX_FLOOR + 1)));
        }
    }

}
