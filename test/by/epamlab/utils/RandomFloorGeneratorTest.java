package by.epamlab.utils;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.CoreMatchers.not;

import org.junit.Test;

/**
 * @author Admin
 *
 */
public class RandomFloorGeneratorTest {

    /**
     *
     */
    @Test
    public void testGetAnotherFloor() {
        for (int i = 0; i < 100; i++) {
            int floor = RandomFloorGenerator.getFloor();
            int anotherFloor = RandomFloorGenerator.getAnotherFloor(floor);
            assertThat("two floor", floor, is(not(anotherFloor)));
        }
    }

}
