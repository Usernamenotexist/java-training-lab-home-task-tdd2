package by.epamlab.utils;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Admin
 *
 */
public class SequenserTest {

    private Sequencer test;
    /**
     * init
     */
    @Before
    public void init() {
        test = new Sequencer();
    }
    /**
     * test
     */
    @Test
    public void testNext() {
        assertThat("next long", test.next(), is(equalTo(0)));
    }

    /**
     *
     */
    @Test
    public void testNext2() {
        int oneNumber = test.next();
        int secondNumber = test.next();
        assertThat("next int", oneNumber, is(equalTo(secondNumber - 1)));
    }

}
